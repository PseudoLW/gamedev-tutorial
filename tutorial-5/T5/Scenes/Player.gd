extends KinematicBody2D

# Trying to make a smooth jump lol

export (int) var max_speed = 1000
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600
export (int) var acceleration = 800
export (float) var drag_factor = 0.95

const UP = Vector2(0,-1)

var velocity = Vector2()
var pressedL = false
var pressedR = false

var animation = 'default'

func get_input():
	if is_on_floor() and Input.is_action_just_pressed('up'):
		velocity.y = jump_speed
	if Input.is_action_just_pressed('left'):
		pressedL = true
	if Input.is_action_just_released("left"):
		pressedL = false
	if Input.is_action_just_pressed('right'):
		pressedR = true
	if Input.is_action_just_released('right'):
		pressedR = false

func _physics_process(delta):
	var dx = 0
	if (pressedL):
		dx -= delta * acceleration
	if (pressedR):
		dx += delta * acceleration

	velocity.x = clamp(velocity.x + dx,
		-max_speed,
		max_speed)
	
	if (sign(velocity.x) != sign(dx)):
		velocity.x *= drag_factor
	if (abs(velocity.x) > 1):
		$AnimatedSprite.scale.x = sign(velocity.x)
	
	if (!is_on_floor()):
		$AnimatedSprite.play('jump')
	elif (abs(velocity.x) > 1):
		$AnimatedSprite.play('run')
	else:
		$AnimatedSprite.play('default')
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)

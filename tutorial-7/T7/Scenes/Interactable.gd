extends Node

class_name Interactable

onready var pointed = false
onready var pointedBuffer = false

func interact():
	pass

func point():
	pointed = true
	pointedBuffer = true

func _process(_delta):
	if pointedBuffer == false:
		pointed = false
	pointedBuffer = false

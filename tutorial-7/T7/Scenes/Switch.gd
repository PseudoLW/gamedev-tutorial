extends Interactable

export var light : NodePath
export var on_by_default = true
export var energy_when_on = 1
export var energy_when_off = 0

onready var light_node = get_node(light)

var on = on_by_default

func _ready():
	light_node.set_param(Light.PARAM_ENERGY, energy_when_on)

func _process(delta):
	._process(delta)
	if pointed:
		$Switch.mesh.surface_get_material(0).albedo_color = 'ff3333'
	else:
		$Switch.mesh.surface_get_material(0).albedo_color = 'ff0000'

func interact():
	on = !on
	var timer = 15
	light_node.set_param(Light.PARAM_ENERGY, energy_when_on if on else energy_when_off)
	
	$Switch.scale.z = 0.8
	while timer > 0:
		yield(get_tree(), "idle_frame")
		timer -= 1
	$Switch.scale.z = 1
	
